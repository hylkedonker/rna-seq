#!/bin/bash
dsub \
    --provider google-cls-v2 \
    --project nihyl-tutorial2 \
    --location europe-west4 \
    --zones europe-west4-a \
    --preemptible \
    --min-ram 8 \
    --min-cores 2 \
    --logging "${BUCKET}/logging/step1/" \
    --image registry.gitlab.com/hylkedonker/rna-seq \
    --tasks step1.tsv \
    --script step1.sh