#!/bin/bash
STAR \
    --runThreadN 4 \
    --readFilesCommand zcat \
    --genomeDir ${GENOME_INDEX} \
    --readFilesIn ${R1} ${R2} \
    --outFileNamePrefix "${OUTPUT}/"